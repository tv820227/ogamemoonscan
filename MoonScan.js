// modify the event id  of the fleet first to track its movement
var event_id= 'eventRow-261076';
query = 'div#' + event_id + '> ul li[class="arrivalTime"]';

function getGameTime(){
    return $('[class="OGameClock"] > span').text();    
}

function refreshScan(){
    document.getElementsByClassName('refreshPhalanxLink tooltip js_hideTipOnMobile overlay fleft tpd-hideOnClickOutside')[0].click();
    console.log('Refreshed at the Game time:', getGameTime(), '\n');
}

function sleep(s) {
    return new Promise(resolve => setTimeout(resolve, s*1000));
}

var origin_time = $(query).text()
var fleet_time = ""
var game_time = "" 
do {
    await sleep(3);
    refreshScan();
    fleet_time = $(query).text();
    game_time = getGameTime();
} while(fleet_time != "");

console.log('Stopped at the Game time:', getGameTime(), '\n');
console.log('Original arriving time:', origin_time);