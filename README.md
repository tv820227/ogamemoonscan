# OGameMoonScan

Automatically refresh a moon scanner until the fleet disappears

1. 進入銀河系
2. 掃描目標星球
3. 按F12
4. 選取左上方選取物件
5. 按下目標艦隊的抵達倒數時間
6. 找到目標艦隊的event id
7. 修改MoonScan.js要的event_id
8. 回到網頁開啟console
9. 手動按一次頁面上的refresh 
10. 將script全部貼上並執行 
